#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <conio.h>
#include <string.h>
#include <ctime>
#include <cmath>

using namespace std;

int main(int argc, char *argv[])
{
	const int KTP    = 1;
	const int SIM    = 2;
	const int PASPOR = 3;

	const int ALPHARD = 1;
	const int AVANZA  = 2;
	const int YARIS   = 3;

	const int HARGA_ALPHARD = 800000;
	const int HARGA_AVANZA  = 500000;
	const int HARGA_YARIS   = 200000;

    int menu = 0;
    
    char no_identitas[21];
    int tipe_identitas;
    char nama[65];
    char jenis_kelamin;
    char alamat[101];
    char kota[51];
    
    int stok_alphard = 10;
    int stok_avanza  = 20;
    int stok_yaris   = 5;
    
    char no_sewa[11];
    char cari_no_sewa[11];
    int pilihan_mobil;
    int tahun_sewa, bulan_sewa, tanggal_sewa, jam_sewa, menit_sewa;
    int tahun_kembali, bulan_kembali, tanggal_kembali, jam_kembali, menit_kembali;
    int harga;
    
    long int total_waktu_sewa;
	long int total_waktu_kembali;
    double total_waktu;
    double total_waktu_jam;
    int denda;
    int total_harga;
    
    do {
        system("cls");
        cout << "Menu Pilihan" << endl;
        cout << "1 . Pendaftaran" << endl;
        cout << "2 . Daftar Mobil" << endl;
        cout << "3 . Penyewaan" << endl;
        cout << "4 . Pengembalian" << endl;
        cout << "5 . Keluar" << endl;
        cout << "Pilih menu ? ";
        cin >> menu;
        switch (menu) {
            case 1:
                system("cls");
                cout << "Pendaftaran" << endl;
                cout << "==============================" << endl;
                 
                cout << "No Identitas \t\t: ";
                fflush(stdin);
                cin.get(no_identitas, 20);
                 
                cout << "Tipe Identitas \t\t: " << endl;
                cout << "1 . KTP" << endl;
                cout << "2 . SIM" << endl;
                cout << "3 . PASPOR" << endl;
                cout << "Pilih tipe identitas ? ";
                cin >> tipe_identitas;
                 
                cout << "Nama \t\t\t: ";
                fflush(stdin);
                cin.get(nama, 64);
                 
                cout << "Jenis Kelamin (L/P) \t: ";
                jenis_kelamin = getche();
                 
                cout << endl << "Alamat \t\t\t: ";
                fflush(stdin);
                cin.get(alamat, 100);

                cout << "Kota \t\t\t: ";
                fflush(stdin);
                cin.get(kota, 50);
                 
                cout << endl << endl << "Tekan enter untuk melanjutkan...";
                getch();
                 
                break;
            case 2:
				system("cls");
				cout << "Daftar Mobil" << endl;
                cout << "==============================" << endl;
                
                cout << "Alphard \t: " << stok_alphard << " unit" << endl;
				cout << "Avanza \t\t: "  << stok_avanza  << " unit" << endl;
				cout << "Yaris \t\t: "   << stok_yaris   << " unit" << endl;

                cout << endl << endl << "Tekan enter untuk melanjutkan...";
                getch();
				
                break;
            case 3:
				system("cls");
				cout << "Penyewaan" << endl;
                cout << "==============================" << endl;
                
                // Input No Sewa
                cout << "No Sewa \t: ";
				fflush(stdin);
				cin.get(no_sewa, 10);
				
				cout << endl << endl;
				
				// Tampil Identitas Penyewa
				cout << "Identitas Penyewa" << endl;
                cout << "==============================" << endl;

				cout << "No Identitas \t: " << no_identitas << endl;
				
				cout << "Tipe Identitas \t: ";
				switch(tipe_identitas) {
					case KTP:
						cout << "KTP" << endl;
						break;
					case SIM:
						cout << "SIM" << endl;
						break;
					case PASPOR:
						cout << "PASPOR" << endl;
						break;
					default:
						cout << "Tidak Terdefinisi" << endl;
				}
				
				cout << "Nama \t\t: " << nama << endl;
				cout << "Jenis Kelamin \t: " << jenis_kelamin << endl;
				cout << "Alamat \t\t: " << alamat << endl;
				cout << "Kota \t\t: " << kota << endl;

				cout << endl << endl;
				
				// Memilih Mobil
				cout << "Pilih Mobil" << endl;
                cout << "==============================" << endl;

				cout << "1 . Alphard" << endl;
				cout << "2 . Avanza" << endl;
				cout << "3 . Yaris" << endl;
				cout << "Pilih ? ";
				cin >> pilihan_mobil;

				cout << endl << endl;

				// Tanggal dan waktu sewa
				cout << "Tanggal dan waktu sewa" << endl;
                cout << "==============================" << endl;

				cout << "Tahun (2015) \t\t: ";
				cin >> tahun_sewa;
				
				cout << "Bulan (11) \t\t: ";
				cin >> bulan_sewa;
				
				cout << "Tanggal (6) \t\t: ";
				cin >> tanggal_sewa;
				
				cout << "Jam (13) \t\t: ";
				cin >> jam_sewa;
				
				cout << "Menit (00) \t\t: ";
				cin >> menit_sewa;
				
				// Pengerangan Stok
				switch(pilihan_mobil) {
					case ALPHARD:
						stok_alphard -= 1;
						break;
					case AVANZA:
						stok_avanza -= 1;
						break;
					case YARIS:
						stok_yaris -= 1;
						break;
				}

                cout << endl << endl << "Tekan enter untuk melanjutkan...";
                getch();
                
                break;
            case 4:
				system("cls");
				cout << "Pengembalian" << endl;
                cout << "==============================" << endl;

                cout << "No Sewa \t: ";
				fflush(stdin);
				cin.get(cari_no_sewa, 10);
				
				if (strcmp(cari_no_sewa, no_sewa) != 0) {
					cout << "Data tidak ditemukan...";
					getch();
					break;
				}

                cout << endl << "==============================" << endl;

				// Data Penyewa
				cout << "No Identitas \t: " << no_identitas << endl;

				cout << "Tipe Identitas \t: ";
				switch(tipe_identitas) {
					case KTP:
						cout << "KTP" << endl;
						break;
					case SIM:
						cout << "SIM" << endl;
						break;
					case PASPOR:
						cout << "PASPOR" << endl;
						break;
					default:
						cout << "Tidak Terdefinisi" << endl;
				}

				cout << "Nama \t\t: " << nama << endl;
				cout << "Jenis Kelamin \t: " << jenis_kelamin << endl;
				cout << "Alamat \t\t: " << alamat << endl;
				cout << "Kota \t\t: " << kota << endl;
				cout << "Mobil \t\t: ";
				
				harga = 0;
				
				switch(pilihan_mobil) {
					case ALPHARD:
						cout << "Alphard" << endl;
						cout << "Harga Sewa \t: Rp " << HARGA_ALPHARD << endl;
						harga += HARGA_ALPHARD;
						break;
					case AVANZA:
						cout << "Avanza" << endl;
						cout << "Harga Sewa \t: Rp " << HARGA_AVANZA << endl;
						harga += HARGA_AVANZA;
						break;
					case YARIS:
						cout << "Yaris" << endl;
						cout << "Harga Sewa \t: Rp " << HARGA_YARIS << endl;
						harga += HARGA_YARIS;
						break;
					default:
						cout << "Tidak Terdefinisi" << endl;
				}
				
                cout << "==============================" << endl << endl;
                
                // Tanggal dan waktu kembali
				cout << "Tanggal dan waktu kembali" << endl;
                cout << "==============================" << endl;

				cout << "Tahun (2015) \t\t: ";
				cin >> tahun_kembali;

				cout << "Bulan (11) \t\t: ";
				cin >> bulan_kembali;

				cout << "Tanggal (7) \t\t: ";
				cin >> tanggal_kembali;

				cout << "Jam (13) \t\t: ";
				cin >> jam_kembali;

				cout << "Menit (20) \t\t: ";
				cin >> menit_kembali;

				total_waktu_sewa  = 0;
				total_waktu_sewa += menit_sewa   * 60;
				total_waktu_sewa += jam_sewa     * 60 * 60;
				total_waktu_sewa += tanggal_sewa * 60 * 60 * 24;
				total_waktu_sewa += bulan_sewa   * 60 * 60 * 24 * 30;
				total_waktu_sewa += tahun_sewa   * 60 * 60 * 24 * 365;

				total_waktu_kembali  = 0;
				total_waktu_kembali += menit_kembali   * 60;
				total_waktu_kembali += jam_kembali     * 60 * 60;
				total_waktu_kembali += tanggal_kembali * 60 * 60 * 24;
				total_waktu_kembali += bulan_kembali   * 60 * 60 * 24 * 30;
				total_waktu_kembali += tahun_kembali   * 60 * 60 * 24 * 365;
				
				total_waktu = total_waktu_kembali - total_waktu_sewa;
				total_waktu_jam = ceil(total_waktu / 3600);
				denda = 0;
				
				if (total_waktu_jam > 24) {
					total_waktu_jam -= 24;
					denda += total_waktu_jam * 100000;
					cout << endl << "Denda \t\t\t: Rp " << denda << endl;
				}
				
				total_harga = harga + denda;
				
				cout << "Total Harga \t\t: Rp " << total_harga << endl;
				
				
				switch(pilihan_mobil) {
					case ALPHARD:
						stok_alphard += 1;
						break;
					case AVANZA:
						stok_avanza += 1;
						break;
					case YARIS:
						stok_yaris += 1;
						break;
				}
				
                cout << endl << endl << "Tekan enter untuk melanjutkan...";
                getch();
				
                break;
            case 5:
                // Do Nothing
                break;
            default:
                cout << "Menu tidak terdefinisi" << endl << endl;
                cout << "Tekan enter untuk melanjutkan...";
                getche();
        }
    } while (menu != 5);
    return EXIT_SUCCESS;
}
